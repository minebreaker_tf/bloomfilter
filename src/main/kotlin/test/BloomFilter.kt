package test

import java.util.*

/**
 * BloomFilter class.
 */
class BloomFilter {

    private val filter: BitSet = BitSet(65535)

    /**
     * Add object to the filter.
     *
     * @param arg Object added.
     */
    fun add(arg: Any) {
        val hash = arg.hashCode()
        val hash1 = hash and 0xFFFF
        val hash2 = hash shr 16

        // set bits of the filter at hashed index
        filter.set(hash1)
        filter.set(hash2)
    }

    /**
     * Check if the [arg] is in the filter.
     *
     * @param arg Object to be checked.
     * @return true if [arg] is in the filter.
     */
    fun contains(arg: Any): Boolean {
        val hash = arg.hashCode()
        val hash1 = hash and 0xFFFF
        val hash2 = hash shr 16

        return filter.get(hash1) && filter.get(hash2)
    }

}
