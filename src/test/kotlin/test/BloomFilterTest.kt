package test

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class BloomFilterTest {

    var filter: BloomFilter = BloomFilter()

    @Before
    fun setUp() {
        filter = BloomFilter()
    }

    @Test
    fun test() {
        filter.add("ABC")
        filter.add("DEF")
        filter.add("GHI")
        filter.add("JKL")
        filter.add("MNO")
        filter.add("PQR")

        assertTrue(filter.contains("ABC"))
        assertTrue(filter.contains("DEF"))
        assertTrue(filter.contains("GHI"))
        assertTrue(filter.contains("JKL"))
        assertTrue(filter.contains("MNO"))
        assertTrue(filter.contains("PQR"))
        assertFalse(filter.contains("123"))
        assertFalse(filter.contains("456"))
        assertFalse(filter.contains("789"))
        assertFalse(filter.contains("000"))
    }

}
